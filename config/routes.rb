GlbWebApp::Application.routes.draw do

  root :to => 'home#index'

  get 'providers_confirm' => 'home#providers_confirm'
  get 'new_mailing' => 'home#new_mailing'
  get 'mailing_list' => 'home#mailing_list'
  get 'schedule_batch_1' => 'home#schedule_batch_1'
  get 'schedule_batch_2' => 'home#schedule_batch_2'
  get 'job_preview' => 'home#job_preview'

	# part 2

  get 'new_mailing_confirm' => 'home#new_mailing_confirm'
  get 'inbox/list' => 'home#inbox_list'
  get 'inbox/grid' => 'home#inbox_grid'
  get 'received' => 'home#received'
  get 'providers' => 'home#providers'
  get 'login/1' => 'home#login_1'
  get 'login/2' => 'home#login_2'

  # part 3

  get 'profile' => 'home#profile'
  get 'new_address' => 'home#new_address'
  get 'new_address_confirm' => 'home#new_address_confirm'
  get 'change_password' => 'home#change_password'
  get 'edit_name' => 'home#change_name'
  get 'preferences' => 'home#preferences'
  get 'notifications' => 'home#notifications'
  get 'add_filter' => 'home#add_filter'
  get 'edit_filter' => 'home#edit_filter'

  get 'new_header' => 'home#new_header'

  # part4
  get 'address' => 'home#address'

end
