class HomeController < ApplicationController

	def index
		@title = "Index"
		@header_icons = true
	end

	def providers_confirm
		@breadcrumbs = ["Home", "Preferences", "Providers"]
		@title = "Confirmation"
	end

	def new_mailing
		@breadcrumbs = ["Home", "New mailing"]
		@title = "New mailing"
	end

	def mailing_list
		@breadcrumbs = ["Home", "Mailing list"]
		@title = "Mailing list"
	end

	def schedule_batch_1
		@breadcrumbs = ["Home", "Mailing list", "Schedule batches"]
		@title = "Schedule batch"
	end

	def schedule_batch_2
		@breadcrumbs = ["Home", "Mailing list", "Schedule batches"]
		@title = "Schedule batch"
	end

	def job_preview
		@breadcrumbs = ["Home", "Mailing list", "Mailing #01"]
		@title = "Mailing #01"
	end

	# part 2

	def new_mailing_confirm
		@breadcrumbs = ["Home", "New mailing"]
		@title = "New mailing"
	end

	def inbox_list
		@breadcrumbs = ["Home", "Inbox", "Banks"]
		@title = "Inbox"
	end

	def inbox_grid
		@breadcrumbs = ["Home", "Inbox", "Banks"]
		@title = "Inbox"
	end

	def received
		@breadcrumbs = ["Home", "Inbox", "Received", "Sample title"]
		@title = "Received"
	end

	def providers
		@breadcrumbs = ["Home", "Preferences", "Providers"]
		@title = "Providers list"
	end

	def login_1
		@title = "Login"
	end

	def login_2
		@title = "Login"
	end

	# part 3

	def profile
		@breadcrumbs = ["Home", "Preferences", "Profile"]
		@title = "Profile"
	end

	def new_address
		@breadcrumbs = ["Home", "Profile", "New postal addresses"]
		@title = "Postal addresses"
	end

	def new_address_confirm
		@breadcrumbs = ["Home", "Profile", "New postal addresses"]
		@title = "Postal addresses"
	end

	def change_password
		@breadcrumbs = ["Home", "Profile", "Change password"]
		@title = "Password"
	end

	def change_name
		@breadcrumbs = ["Home", "Profile", "Edit name"]
		@title = "Edit name"
	end

	def preferences
		@breadcrumbs = ["Home", "Preferences"]
		@title = "Preferences"
	end

	def notifications
		@breadcrumbs = ["Home", "Partials"]
		@title = "Notifications"
	end

	def add_filter
		@breadcrumbs = ["Home", "Preferences", "Add filter"]
		@title = "Add filter"
	end

	def edit_filter
		@breadcrumbs = ["Home", "Preferences", "Edit filter"]
		@title = "Edit filter"
	end

	def new_header
		@new_menu = true
  end

  # part 3

  def address
    @breadcrumbs = ["Home", "Address"]
    @title = "Address"

  end

end