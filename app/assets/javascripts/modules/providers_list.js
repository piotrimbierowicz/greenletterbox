$(document).ready(function() {
	$('.providers-group').each( function() {
		var selected = $(this).find('.selected')
		var list = $(this).find('.provider')
		list.find('input').change( function(e) {
			checked = e.currentTarget.checked
			if(checked)
			{
				$(this).parent().addClass('selected')
				selected.html( parseInt(selected.html()) + 1 )
			}
			else
			{
				$(this).parent().removeClass('selected')
				selected.html( parseInt(selected.html()) - 1 )
			}
		})
		var search = $(this).find('.search')
		search.keyup( function() {
			phrase = $(this).val()
			matches = 0
			list.each( function() {
				if( $(this).html().match(phrase) )
				{
					matches+=1
					$(this).css('display', 'block')
				}
				else
				{
					$(this).css('display', 'none')

				}
			})
			if(matches == 0)
			{
				search.addClass('no-matches')
			}
			else
			{
				search.removeClass('no-matches')
			}
		})
		
	})
})