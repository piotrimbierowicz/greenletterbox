$(function() {
	home_promo_resize = function() {
		text_height = parseInt($('.home-promo .row').height())
		$('.home-promo .row').css('margin-top', text_height/-2)
		image_height = parseInt($('.home-promo-background img').height())
		if(text_height < image_height)
		{
			$('.home-promo').css('height', image_height)
		}
		else
		{
			$('.home-promo').css('height', text_height+40)
		}
	}
	$(window).load( function() {
		home_promo_resize()
	})
	$(window).resize( function() {
		home_promo_resize()
	})
})