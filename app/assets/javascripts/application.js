// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require_tree ./modules

$(window).load(function(){
	$(".scrollarea").mCustomScrollbar({
		scrollButtons:{
			enable:true
		}
	});
});

$(document).ready( function() {


	$('.checkbox-ui').button();
	$('.select-ui').customSelect();
	$('.selectable-ui').button();


	function fitPage()
	{
		$('.main').removeAttr('style')
		var windowHeight = parseInt($(window).height());
		var pageHeight = parseInt($('body').height());
		var diff = windowHeight - pageHeight;
		if( diff > 0 )
		{
			if( $('.error-container').size() > 0 ) //this is an error page
			{
				$('.error-container .container').css('padding-bottom', diff/2 );
				$('.error-container .container').css('padding-top', diff - diff/2 );
			}
			else if( $('.main.form-page').size() > 0)
			{
				diff = diff + parseInt($('.main').css('padding-bottom')) + parseInt($('.main').css('padding-top'))
				$('.main').css('padding-top', diff/3 )
				$('.main').css('padding-bottom', diff - diff/3 )
			}
			else 
			{
				diff = diff + parseInt($('.main').css('padding-bottom'))
				$('.main').css('padding-bottom', diff )
			}
		}
	}

	fitPage()

	$(window).resize( function() {
		fitPage()
	})
})
